import sys, os
from cx_Freeze import setup, Executable

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"
#sys.path.append(os.path.abspath("src"))
# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "packages": ["os", "sip"],
    "include_files": ["lang_table.db"]
    #"icon": "ui/icons/big_ui/srt_512.png"
}
e = Executable(
    script="run.py",
    base=base,
    targetName="SubFetcher.exe",
    icon="ui/icons/big_ui/srt_512.ico"
)

setup(
    name="subFetcher",
    version="0.1",
    author="Marcello Massaro",
    description="A subtitles downloader!",
    options={"build_exe": build_exe_options},
    executables=[e]
)