# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/mainWindow.ui'
#
# Created: Sun Jan 11 22:57:55 2015
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from src import res_rc


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setMinimumSize(QtCore.QSize(800, 600))
        MainWindow.setMaximumSize(QtCore.QSize(800, 600))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/big_ui/srt_512.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.searchTab = QtWidgets.QWidget()
        self.searchTab.setObjectName("searchTab")
        self.gridLayout = QtWidgets.QGridLayout(self.searchTab)
        self.gridLayout.setObjectName("gridLayout")
        self.searchBarLayout = QtWidgets.QHBoxLayout()
        self.searchBarLayout.setObjectName("searchBarLayout")
        self.searchLine = QtWidgets.QLineEdit(self.searchTab)
        self.searchLine.setObjectName("searchLine")
        self.searchBarLayout.addWidget(self.searchLine)
        self.btnBrowseSearchFolder = QtWidgets.QPushButton(self.searchTab)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icons/icons/small_ui/folder.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnBrowseSearchFolder.setIcon(icon1)
        self.btnBrowseSearchFolder.setObjectName("btnBrowseSearchFolder")
        self.searchBarLayout.addWidget(self.btnBrowseSearchFolder)
        self.gridLayout.addLayout(self.searchBarLayout, 0, 0, 1, 1)
        self.btnSearch = QtWidgets.QPushButton(self.searchTab)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icons/icons/small_ui/magnifier.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnSearch.setIcon(icon2)
        self.btnSearch.setDefault(True)
        self.btnSearch.setObjectName("btnSearch")
        self.gridLayout.addWidget(self.btnSearch, 0, 1, 1, 1)
        self.infoBox = QtWidgets.QGroupBox(self.searchTab)
        self.infoBox.setAlignment(QtCore.Qt.AlignCenter)
        self.infoBox.setFlat(False)
        self.infoBox.setObjectName("infoBox")
        self.formLayout = QtWidgets.QFormLayout(self.infoBox)
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.formLayout.setFormAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.formLayout.setObjectName("formLayout")
        self.lblSeries = QtWidgets.QLabel(self.infoBox)
        self.lblSeries.setTextFormat(QtCore.Qt.AutoText)
        self.lblSeries.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblSeries.setObjectName("lblSeries")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.lblSeries)
        self.lblSeriesText = QtWidgets.QLabel(self.infoBox)
        self.lblSeriesText.setText("")
        self.lblSeriesText.setObjectName("lblSeriesText")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lblSeriesText)
        self.lblSeason = QtWidgets.QLabel(self.infoBox)
        self.lblSeason.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblSeason.setObjectName("lblSeason")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.lblSeason)
        self.lblSeasonText = QtWidgets.QLabel(self.infoBox)
        self.lblSeasonText.setText("")
        self.lblSeasonText.setObjectName("lblSeasonText")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lblSeasonText)
        self.lblEpisode = QtWidgets.QLabel(self.infoBox)
        self.lblEpisode.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblEpisode.setObjectName("lblEpisode")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.lblEpisode)
        self.lblEpisodeText = QtWidgets.QLabel(self.infoBox)
        self.lblEpisodeText.setText("")
        self.lblEpisodeText.setObjectName("lblEpisodeText")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lblEpisodeText)
        self.lblTitle = QtWidgets.QLabel(self.infoBox)
        self.lblTitle.setObjectName("lblTitle")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.lblTitle)
        self.lblTitleText = QtWidgets.QLabel(self.infoBox)
        self.lblTitleText.setText("")
        self.lblTitleText.setObjectName("lblTitleText")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.lblTitleText)
        self.lblIMDB = QtWidgets.QLabel(self.infoBox)
        self.lblIMDB.setObjectName("lblIMDB")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.lblIMDB)
        self.lblIMDBText = QtWidgets.QLabel(self.infoBox)
        self.lblIMDBText.setText("")
        self.lblIMDBText.setWordWrap(True)
        self.lblIMDBText.setOpenExternalLinks(True)
        self.lblIMDBText.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.lblIMDBText.setObjectName("lblIMDBText")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.lblIMDBText)
        self.gridLayout.addWidget(self.infoBox, 1, 1, 1, 1)
        self.actionsBox = QtWidgets.QGroupBox(self.searchTab)
        self.actionsBox.setAlignment(QtCore.Qt.AlignCenter)
        self.actionsBox.setObjectName("actionsBox")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.actionsBox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.btnDownloadSelected = QtWidgets.QPushButton(self.actionsBox)
        self.btnDownloadSelected.setObjectName("btnDownloadSelected")
        self.verticalLayout_2.addWidget(self.btnDownloadSelected)
        self.btnDownloadAll = QtWidgets.QPushButton(self.actionsBox)
        self.btnDownloadAll.setObjectName("btnDownloadAll")
        self.verticalLayout_2.addWidget(self.btnDownloadAll)
        self.gridLayout.addWidget(self.actionsBox, 2, 1, 1, 1)
        self.tableView = QtWidgets.QTableView(self.searchTab)
        self.tableView.setAlternatingRowColors(True)
        self.tableView.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableView.setShowGrid(False)
        self.tableView.setSortingEnabled(True)
        self.tableView.setObjectName("tableView")
        self.gridLayout.addWidget(self.tableView, 1, 0, 2, 1)
        self.gridLayout.setColumnStretch(0, 7)
        self.gridLayout.setColumnStretch(1, 3)
        self.gridLayout.setRowStretch(0, 1)
        self.gridLayout.setRowStretch(1, 7)
        self.gridLayout.setRowStretch(2, 2)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/icons/icons/small_ui/film_edit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tabWidget.addTab(self.searchTab, icon3, "")
        self.settingsTab = QtWidgets.QWidget()
        self.settingsTab.setObjectName("settingsTab")
        self.btnAbout = QtWidgets.QPushButton(self.settingsTab)
        self.btnAbout.setGeometry(QtCore.QRect(620, 10, 141, 23))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnAbout.sizePolicy().hasHeightForWidth())
        self.btnAbout.setSizePolicy(sizePolicy)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/icons/icons/small_ui/help.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnAbout.setIcon(icon4)
        self.btnAbout.setObjectName("btnAbout")
        self.languageSettingsGroupBox = QtWidgets.QGroupBox(self.settingsTab)
        self.languageSettingsGroupBox.setGeometry(QtCore.QRect(10, 40, 751, 81))
        self.languageSettingsGroupBox.setObjectName("languageSettingsGroupBox")
        self.formLayout_2 = QtWidgets.QFormLayout(self.languageSettingsGroupBox)
        self.formLayout_2.setObjectName("formLayout_2")
        self.lblSubtitlesLanguage = QtWidgets.QLabel(self.languageSettingsGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblSubtitlesLanguage.sizePolicy().hasHeightForWidth())
        self.lblSubtitlesLanguage.setSizePolicy(sizePolicy)
        self.lblSubtitlesLanguage.setScaledContents(False)
        self.lblSubtitlesLanguage.setObjectName("lblSubtitlesLanguage")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.lblSubtitlesLanguage)
        self.subtitlesLanguageComboBox = QtWidgets.QComboBox(self.languageSettingsGroupBox)
        self.subtitlesLanguageComboBox.setObjectName("subtitlesLanguageComboBox")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.subtitlesLanguageComboBox)
        self.lblAppLanguage = QtWidgets.QLabel(self.languageSettingsGroupBox)
        self.lblAppLanguage.setObjectName("lblAppLanguage")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.lblAppLanguage)
        self.appLanguageComboBox = QtWidgets.QComboBox(self.languageSettingsGroupBox)
        self.appLanguageComboBox.setObjectName("appLanguageComboBox")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.appLanguageComboBox)
        self.folderSettingsGroupBox = QtWidgets.QGroupBox(self.settingsTab)
        self.folderSettingsGroupBox.setGeometry(QtCore.QRect(229, 129, 531, 131))
        self.folderSettingsGroupBox.setObjectName("folderSettingsGroupBox")
        self.rbtnSamePathAsVideo = QtWidgets.QRadioButton(self.folderSettingsGroupBox)
        self.rbtnSamePathAsVideo.setGeometry(QtCore.QRect(20, 20, 261, 17))
        self.rbtnSamePathAsVideo.setChecked(False)
        self.rbtnSamePathAsVideo.setObjectName("rbtnSamePathAsVideo")
        self.saveFolderBtnGroup = QtWidgets.QButtonGroup(MainWindow)
        self.saveFolderBtnGroup.setObjectName("saveFolderBtnGroup")
        self.saveFolderBtnGroup.addButton(self.rbtnSamePathAsVideo)
        self.rbtnSelectedPath = QtWidgets.QRadioButton(self.folderSettingsGroupBox)
        self.rbtnSelectedPath.setGeometry(QtCore.QRect(20, 40, 261, 17))
        self.rbtnSelectedPath.setChecked(True)
        self.rbtnSelectedPath.setObjectName("rbtnSelectedPath")
        self.saveFolderBtnGroup.addButton(self.rbtnSelectedPath)
        self.layoutWidget = QtWidgets.QWidget(self.folderSettingsGroupBox)
        self.layoutWidget.setGeometry(QtCore.QRect(40, 60, 481, 26))
        self.layoutWidget.setObjectName("layoutWidget")
        self.saveFolderLayout = QtWidgets.QHBoxLayout(self.layoutWidget)
        self.saveFolderLayout.setContentsMargins(0, 0, 0, 0)
        self.saveFolderLayout.setObjectName("saveFolderLayout")
        self.saveFolderLine = QtWidgets.QLineEdit(self.layoutWidget)
        self.saveFolderLine.setObjectName("saveFolderLine")
        self.saveFolderLayout.addWidget(self.saveFolderLine)
        self.btnBrowseSaveFolder = QtWidgets.QPushButton(self.layoutWidget)
        self.btnBrowseSaveFolder.setIcon(icon1)
        self.btnBrowseSaveFolder.setObjectName("btnBrowseSaveFolder")
        self.saveFolderLayout.addWidget(self.btnBrowseSaveFolder)
        self.logInGroupBox = QtWidgets.QGroupBox(self.settingsTab)
        self.logInGroupBox.setGeometry(QtCore.QRect(10, 130, 211, 131))
        self.logInGroupBox.setObjectName("logInGroupBox")
        self.formLayout_3 = QtWidgets.QFormLayout(self.logInGroupBox)
        self.formLayout_3.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout_3.setObjectName("formLayout_3")
        self.lblUsername = QtWidgets.QLabel(self.logInGroupBox)
        self.lblUsername.setObjectName("lblUsername")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.lblUsername)
        self.usernameLine = QtWidgets.QLineEdit(self.logInGroupBox)
        self.usernameLine.setObjectName("usernameLine")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.usernameLine)
        self.lblPassword = QtWidgets.QLabel(self.logInGroupBox)
        self.lblPassword.setObjectName("lblPassword")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.lblPassword)
        self.passwordLine = QtWidgets.QLineEdit(self.logInGroupBox)
        self.passwordLine.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passwordLine.setObjectName("passwordLine")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.passwordLine)
        self.btnLogIn = QtWidgets.QPushButton(self.logInGroupBox)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/icons/icons/small_ui/door_in.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnLogIn.setIcon(icon5)
        self.btnLogIn.setObjectName("btnLogIn")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.btnLogIn)
        self.lblLogInResultIcon = QtWidgets.QLabel(self.logInGroupBox)
        self.lblLogInResultIcon.setMinimumSize(QtCore.QSize(16, 16))
        self.lblLogInResultIcon.setText("")
        self.lblLogInResultIcon.setObjectName("lblLogInResultIcon")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.lblLogInResultIcon)
        self.rememberMeCheclBox = QtWidgets.QCheckBox(self.logInGroupBox)
        self.rememberMeCheclBox.setObjectName("rememberMeCheclBox")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.rememberMeCheclBox)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/icons/icons/small_ui/wrench.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tabWidget.addTab(self.settingsTab, icon6, "")
        self.verticalLayout.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Sub Fetcher"))
        self.searchLine.setPlaceholderText(_translate("MainWindow", "Percorso alla cartella da analizzare"))
        self.btnBrowseSearchFolder.setText(_translate("MainWindow", "Sfoglia..."))
        self.btnSearch.setText(_translate("MainWindow", "Cerca"))
        self.infoBox.setTitle(_translate("MainWindow", "Informazioni Sottotitolo"))
        self.lblSeries.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Serie:</span></p></body></html>"))
        self.lblSeason.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Stagione:</span></p></body></html>"))
        self.lblEpisode.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Episodio:</span></p></body></html>"))
        self.lblTitle.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Titolo:</span></p></body></html>"))
        self.lblIMDB.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Link IMDB:</span></p></body></html>"))
        self.actionsBox.setTitle(_translate("MainWindow", "Azioni"))
        self.btnDownloadSelected.setText(_translate("MainWindow", "Scarica selezione"))
        self.btnDownloadAll.setText(_translate("MainWindow", "Scarica tutto"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.searchTab), _translate("MainWindow", "Cerca sottotitoli"))
        self.btnAbout.setToolTip(_translate("MainWindow", "<html><head/><body><p>Scopri chi ha creato Sub Fetcher e chi indirettamente lo ha reso possibile!</p></body></html>"))
        self.btnAbout.setText(_translate("MainWindow", "Riguardo SubFetcher"))
        self.languageSettingsGroupBox.setTitle(_translate("MainWindow", "Impostazioni lingua"))
        self.lblSubtitlesLanguage.setText(_translate("MainWindow", "Cerca sottotitoli in"))
        self.lblAppLanguage.setText(_translate("MainWindow", "Lingua applicazione"))
        self.folderSettingsGroupBox.setTitle(_translate("MainWindow", "Percorso sottotitoli"))
        self.rbtnSamePathAsVideo.setText(_translate("MainWindow", "Salva nello stesso percorso del file video trovato"))
        self.rbtnSelectedPath.setText(_translate("MainWindow", "Salva tutti i sottotitoli nella cartella selezionata"))
        self.btnBrowseSaveFolder.setText(_translate("MainWindow", "Sfoglia..."))
        self.logInGroupBox.setTitle(_translate("MainWindow", "Log in su OpenSubtitles.org"))
        self.lblUsername.setText(_translate("MainWindow", "Username"))
        self.lblPassword.setText(_translate("MainWindow", "Password"))
        self.btnLogIn.setText(_translate("MainWindow", "Connetti"))
        self.rememberMeCheclBox.setText(_translate("MainWindow", "Ricordami"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.settingsTab), _translate("MainWindow", "Impostazioni"))