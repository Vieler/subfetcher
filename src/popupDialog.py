# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\popupDialog.ui'
#
# Created: Sun Dec  7 14:02:24 2014
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from src import res_rc

class Ui_popupDialog(object):
    def setupUi(self, popupDialog):
        popupDialog.setObjectName("popupDialog")
        popupDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        popupDialog.resize(386, 170)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(popupDialog.sizePolicy().hasHeightForWidth())
        popupDialog.setSizePolicy(sizePolicy)
        popupDialog.setMinimumSize(QtCore.QSize(350, 170))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/misc/icons/big_ui/srt_512.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        popupDialog.setWindowIcon(icon)
        popupDialog.setModal(True)
        self.gridLayout = QtWidgets.QGridLayout(popupDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.lblIcon = QtWidgets.QLabel(popupDialog)
        self.lblIcon.setMinimumSize(QtCore.QSize(128, 128))
        self.lblIcon.setText("")
        self.lblIcon.setAlignment(QtCore.Qt.AlignCenter)
        self.lblIcon.setObjectName("lblIcon")
        self.gridLayout.addWidget(self.lblIcon, 0, 0, 1, 1)
        self.lblText = QtWidgets.QLabel(popupDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblText.sizePolicy().hasHeightForWidth())
        self.lblText.setSizePolicy(sizePolicy)
        self.lblText.setWordWrap(True)
        self.lblText.setObjectName("lblText")
        self.gridLayout.addWidget(self.lblText, 0, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(popupDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 2)
        self.gridLayout.setColumnMinimumWidth(0, 64)
        self.gridLayout.setRowMinimumHeight(0, 64)
        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 3)

        self.retranslateUi(popupDialog)
        self.buttonBox.accepted.connect(popupDialog.accept)
        self.buttonBox.rejected.connect(popupDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(popupDialog)

    def retranslateUi(self, popupDialog):
        _translate = QtCore.QCoreApplication.translate
        popupDialog.setWindowTitle(_translate("popupDialog", "Errore"))
        self.lblText.setText(_translate("popupDialog", "TextLabel"))

