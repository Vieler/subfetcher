#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
subFetcher - your subtitles' best friend!

This is a Python 3.4+ small application written with the aid of PyQt5 for the GUI part.
Its main job is to crawl through a tree of directories starting from a given root and scan
for your typical video file name type for tv shows like

    bestofbest.s01e03.asda[mybesteam].whoknowswhat.potato.mp4

It uses the opensubtitles.org APIs to retrieve video files basic information and the
gzipped subtitles themselves.

This is a fully functional application, though I've written it just for learning purposes.
Any bug reports are appreciated at bitbucket_dot_org_slash_Vieler_slash_subfetcher

A note on code-style/development: docstrings are somewhat coded in reST

This code is licensed under GNU GPL v3 and released without any warranty.
"""
import sys
import os
sys.path.append(os.getcwd()+"/src")
import re
import struct
import binascii
from operator import itemgetter
import gzip
from datetime import datetime
import sqlite3
import json
import hashlib
import urllib.request
import xmlrpc.client

from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import *

from .mainWindow import Ui_MainWindow
from .aboutWindow import Ui_aboutWindow
from .popupDialog import Ui_popupDialog


# AUXILIARY PIECES ##################################
def log(msg, type, err_msg="", err_code="", log_file_name=os.getcwd()+"/subFetcher.log"):
    """
    Logs a line to a predefined file in the application start directory

    :type msg: str
    :param msg: human readable message, verbose
    :type type: str
    :param type: should be one of ERR, WARN, INFO
    :type err_msg: str
    :param err_msg: if the raised exception has a specific message, show it
    :type err_code: str
    :param err_code: if the raised exception has a specific code, show it
    :type log_file_name: str
    :param log_file_name: the static log file name
    :return: nothing
    :rtype: None

    """
    if os.path.isfile(log_file_name):
        if os.path.getsize(log_file_name) > 1048576:
            os.remove(log_file_name)
    log_file = open(log_file_name, "a") if os.path.isfile(log_file_name) else open(log_file_name, "w")
    log_line = "[%s]" % datetime.utcnow().isoformat(' ')
    log_line += "[%s]" % type
    if err_code != "" and err_msg != "":
        log_line += "[ERR_CODE: %s] %s\n\t[ERR_MSG]: %s\n" % (err_code, msg, err_msg)
    else:
        log_line += " %s\n" % msg
    log_file.write(log_line)
    log_file.close()
    return None


def is_online():
    try:
        r = urllib.request.urlopen("http://74.125.228.100", timeout=1)
        return True
    except urllib.request.URLError:
        return False


class SearchWorker(QtCore.QObject):
    update_statusbar = QtCore.pyqtSignal(str)
    scan_complete = QtCore.pyqtSignal()

    def __init__(self, folder,  parent, p=None, *args, **kwargs):
        QtCore.QObject.__init__(self, p)
        self.folder = folder
        self.args = args
        self.kwargs = kwargs
        self.parent = parent
        self.query = []
        self.__tr = QtCore.QCoreApplication.translate

    def run(self):
        self.query = []
        self.__scan_folder(self.folder)
        if is_online():
            if self.query != []:
                q_length = len(self.query)
                chunk = 50
                for i in range(0, q_length, chunk):
                    self.__retrieve_data_from_query(self.query[i:i+chunk])
            self.scan_complete.emit()
        else:
            p = SubFetcherPopupWindow(
                msg=self.__tr("conn_absent", "Connessione assente. Controlla il collegamento"),
                type="i",
                parent=self.parent
            )
            p.show()
        return None

    def __scan_folder(self, folder):
        """From the given folder, tries to build a query of movies/shows' episodes

        This is a recursive function which starts from a given directory and ALWAYS finishes there.
        It avoids files smaller than 8MB (hardcoded), given that it should look for movies and
        similar file types.

        Since it's recursive, returning a value would be complicated. We use an internal variable
        to store the data.

        :param folder: the folder to scan
        :type folder: str
        :return: Nothing
        :rtype: None
        """
        pattern = re.compile("([\w\W\d\D\.\\[\\]]*)s?([0-9]+)[x|e]([0-9]+)([\\[\\]\w\W\d\D\.]*)", re.IGNORECASE)
        # Start directory crawling
        old_directory = os.getcwd()
        os.chdir(folder)
        self.update_statusbar.emit(self.__tr("enter_foldertoscan", "Analizzo %s" % folder))

        for file in os.listdir(os.getcwd()):
            if os.path.isdir(file):
                self.__scan_folder(file)
            # We look for files bigger than 8MB approx.
            elif os.path.getsize(file) > 1024*1024*100:
                # First try to search for subs with hash sum and movie size
                file_hash = hashfile(file)
                matched_filename = re.match(pattern, file)
                if matched_filename is not None:
                    self.query.append(
                        {
                            "sublanguageid": self.parent.subtitles_language,
                            "moviehash": file_hash,
                            "moviebytesize": str(os.path.getsize(file)),
                            "season": matched_filename.group(2),
                            "episode": matched_filename.group(3)
                        }
                    )
                    self.parent.scanned_videos[file_hash] = {
                        "MovieByteSize": str(os.path.getsize(file)),
                        "IDMovieImdb": "",
                        "MovieName": "",
                        "SeriesSeason": "",
                        "SeriesEpisode": "",
                        "IDSubtitleFile": "",
                        "LanguageName": "",
                        "SubLanguageID": "",
                        "OriginalPath": os.path.abspath(file)
                    }
                else:
                    log("File %s didn't match regexp %s" %
                        (file, r"([\w\W\d\D\.]*)s([0-9]+)(x|e)([0-9]+)([\w\W\d\D\.]*)"), type="WARN")
        os.chdir(old_directory)
        return None

    def __retrieve_data_from_query(self, query):
        """

        :param query: list of dictionaries, each of which is a query-type specified by opensubtitles apis
        :type query: list
        :return: nothing
        :rtype: bool
        """
        try:
            results = self.parent.connection.SearchSubtitles(self.parent.connection_token, query)
            results = results["data"]
        except xmlrpc.client.ProtocolError as e:
            p = SubFetcherPopupWindow(self.__tr("conn_error", "Errore nel cercare i sottotitoli. Riprovare più tardi",),
                                      'e', self.parent)
            p.show()
            log("Error while trying to retrieve data from OS.org", "ERR", e.errmsg, e.errcode)
        else:
            if results is not False:
                # Multiple subs are found for given moviehash and moviebytesize
                # so we use this to skip duplicates
                prev_id = ""
                for video_data in results:
                    if video_data["IDMovieImdb"] == prev_id:
                        pass
                    elif (
                        video_data["MovieHash"] in self.parent.scanned_videos and
                        video_data["MovieByteSize"] == self.parent.scanned_videos[video_data["MovieHash"]]["MovieByteSize"]
                    ):
                        h = video_data["MovieHash"]
                        self.parent.at_least_one_match = True
                        self.parent.scanned_videos[h]["IDMovieImdb"] = prev_id = video_data['IDMovieImdb']
                        # TV Shows name are in the form
                        # <"SHOW_NAME" EPISODE_NAME>
                        self.parent.scanned_videos[h]["MovieNameSeries"] = video_data["MovieName"].split('"')[1]
                        self.parent.scanned_videos[h]["SeriesEpisodeTitle"] = video_data["MovieName"].split('"')[2].strip()
                        self.parent.scanned_videos[h]["SeriesSeason"] = int(video_data["SeriesSeason"])
                        self.parent.scanned_videos[h]["SeriesEpisode"] = int(video_data["SeriesEpisode"])
                        self.parent.scanned_videos[h]["IDSubtitleFile"] = video_data["IDSubtitleFile"]
                        self.parent.scanned_videos[h]["LanguageName"] = video_data["LanguageName"]
                        self.parent.scanned_videos[h]["SubLanguageID"] = video_data["SubLanguageID"]
            return True


# UI SPECIFIC AND MAIN APPLICATION ##################
class SubListModel(QtCore.QAbstractTableModel):
    def __init__(self, videos=None, parent=None):
        """

        :param videos: list of dict, containing gathered info on files
        :type videos: list of [dict]
        :param parent: parent of the model (needed only if the model is hierarchical
        :type parent: QObject
        :return: nothing
        """
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.__tr = QtCore.QCoreApplication.translate
        self.video_list = videos

    def rowCount(self, parent=None):
        return len(self.video_list)

    def columnCount(self, parent=None):
        # This is a fixed number and it corresponds to the only information that we want to display to make sure
        # we are downloading the right subs
        return 5

    def insertRow(self, row, parent, data: dict):
        self.beginInsertRows(QtCore.QModelIndex(), row, row)
        self.video_list.insert(row, data)
        self.endInsertRows()
        return True

    def insertRows(self, pos, rows, parent, data: dict):
        # The first arg to insertrows is needed for hierarchical structures
        # so we give an empty QIndexModel. The nex two are the starting
        # position for the editing and the last one which must be zero-based
        # hence the minus one
        self.beginInsertRows(QtCore.QModelIndex(), pos, pos + rows - 1)
        if data is not None:
            for i in range(rows):
                self.video_list.insert(pos, data[i])
            self.endInsertRows()
            return True
        else:
            return False

    def removeRow(self, row, parent=None):
        try:
            self.beginRemoveRows(QtCore.QModelIndex(), row, row)
            del self.video_list[row]
            self.endRemoveRows()
            return True
        except:
            return False

    def removeRows(self, start_row, rows, parent=None):
        self.beginRemoveColumns(QtCore.QModelIndex(), start_row, start_row + rows - 1)
        for i in range(rows):
            del self.video_list[start_row]
        self.endRemoveRows()
        return True

    def sort(self, col, order=None):
        """
        Sorts by column `col` with order `order`

        :param col: column to sort
        :type col: int
        :param order: order type
        :type order: QtCore.Qt.AscendingOrder | QtCore.Qt.AscendingOrder
        :return: nothing
        """
        self.layoutAboutToBeChanged.emit()
        if order == QtCore.Qt.AscendingOrder:
            rev = False
        else:
            rev = True
        if col == 0:
            self.video_list = sorted(self.video_list, key=itemgetter("IDMovieImdb"), reverse=rev)
        elif col == 1:
            self.video_list = sorted(self.video_list, key=itemgetter("MovieName"), reverse=rev)
        elif col == 2:
            self.video_list = sorted(self.video_list, key=itemgetter("MovieNameSeries", "SeriesSeason"), reverse=rev)
        elif col == 3:
            self.video_list = sorted(self.video_list, key=itemgetter("MovieNameSeries", "SeriesSeason", "SeriesEpisode"), reverse=rev)
        self.layoutChanged.emit()

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def data(self, index, role=QtCore.Qt.DisplayRole):
        row = index.row()
        col = index.column()
        # Role to actually display data
        if role == QtCore.Qt.DisplayRole:
            if col == 0:
                if self.video_list[row]["IDMovieImdb"] == "":
                    return self.video_list[row]["OriginalPath"]
                else:
                    return self.video_list[row]["IDMovieImdb"]
            elif col == 1:
                return self.video_list[row]["MovieNameSeries"]
            elif col == 2:
                return self.video_list[row]["SeriesSeason"]
            elif col == 3:
                return self.video_list[row]["SeriesEpisode"]
            elif col == 4:
                return self.video_list[row]["LanguageName"]
        # Role to display an icon next to the item
        if role == QtCore.Qt.DecorationRole:
            #TODO: Cambiare nomi alle bandiere per poterle usare qui
            return None
        # Role for the alignment of the cells
        if role == QtCore.Qt.TextAlignmentRole:
            return QtCore.Qt.AlignCenter

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "IMDB ID"
                elif section == 1:
                    return self.__tr("header", "Titolo")
                elif section == 2:
                    return self.__tr("header", "Stagione")
                elif section == 3:
                    return self.__tr("header", "Episodio")
                elif section == 4:
                    return self.__tr("header", "Lingua")


class SubFetcherMainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, loc="eng", parent=None):
        # UI general setup from QtDesigner generated files
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.__tr = QtCore.QCoreApplication.translate

        # Loading language data form language data dump. Obtained from opensubtitles.org
        if os.path.isfile("lang_table.db"):
            original_db_m5d = "05c79c402f07e36d0e154e64f84e6860"
            if original_db_m5d == md5sum("lang_table.db"):
                pass
            else:
                popup = SubFetcherPopupWindow(self.__tr("md5_fail",
                                                        "<p>Il database dei codici di lingua è corrotto." +
                                                        " Prova a riscaricare l'applicazione.</p>"), 'e', self)
                popup.exec_()
                log("Found corrupted or modified 'lang_table.db'. Application can't start.", "ERR")
                quit()
        else:
            popup = SubFetcherPopupWindow(self.__tr("db_not_present",
                                                    "<p>Il database dei codici di lingua è assente." +
                                                    " Prova a riscaricare l'applicazione.</p>"), 'e', self)
            popup.exec_()
            log("'lang_table.db' not found. Application can't start.", "ERR")
            quit()
        lang_db = sqlite3.connect("lang_table.db")
        lang_db_cursor = lang_db.cursor()
        lang_db_cursor.execute("SELECT * FROM language_codes")
        self.language_codes = {}
        for row in lang_db_cursor.fetchall():
            # The row is in the form (IdSubLanguage, ISO639, language_name, upload_enabled, web_enabled)
            # At the moment, the xml-rpc protocol functions implemented by opensubtitles uses the IdSubLanguage code
            self.language_codes[row[0]] = row[2]
        lang_db.close()
        for k in sorted(self.language_codes.keys()):
            self.subtitlesLanguageComboBox.addItem(self.language_codes[k], k)

        # Reads preferences from json file. It will be rewritten on closing
        self.preferences = {}
        try:
            with open("prefs.json", "r") as f:
                self.preferences = json.loads(f.read())
        except FileNotFoundError:
            log("Preferences file not found. Will be created on exit.", "WARN")

        # Variables initialization
        self.folder_to_scan = r"E:\Video\TV Shows\The Walking Dead\Season 1"  # TODO: cambiare questo prima della release
        self.scanned_videos = {}
        self.app_locale = loc
        self.subtitles_language = (self.preferences["subtitles_language"]
                                   if "subtitles_language" in self.preferences.keys()
                                   else "eng")
        self.subtitles_language_full_name = (self.language_codes[self.preferences["subtitle_language"]]
                                             if "subtitle_language" in self.preferences.keys()
                                             else self.language_codes["eng"])
        self.subtitlesLanguageComboBox.setCurrentIndex(self.subtitlesLanguageComboBox.findData(self.subtitles_language))
        if "save_folder" in self.preferences.keys():
            if self.preferences["save_folder"] is not None:
                self.rbtnSelectedPath.setChecked(True)
                self.saveFolderLine.setText(self.preferences["save_folder"])
                self.saveFolderLine.setEnabled(True)
                self.btnBrowseSaveFolder.setEnabled(True)
            else:
                self.rbtnSamePathAsVideo.setChecked(True)
                self.saveFolderLine.setEnabled(False)
                self.btnBrowseSaveFolder.setEnabled(False)
        else:
            self.rbtnSamePathAsVideo.setChecked(True)
            self.saveFolderLine.setEnabled(False)
            self.btnBrowseSaveFolder.setEnabled(False)
        if "username" in self.preferences.keys():
            self.usernameLine.setText(self.preferences["username"])
        # This is a switch to check for at least one match in the query to the
        # db. Used int he scan_searchline_folder function
        self.at_least_one_match = False
        self.subtitles_model = SubListModel([])
        self.tableView.setModel(self.subtitles_model)
        self.aboutWindow = SubFetcherAboutWindow(self)
        self.searchLine.setText(self.folder_to_scan)

        # OSdb connection and variables
        self.OS_API_address = "http://api.opensubtitles.org/xml-rpc"
        self.OS_user_agent = "OSTestUserAgent"
        self.connection = xmlrpc.client.ServerProxy(self.OS_API_address)
        try:
            self.connection_result = self.log_in("", "")
            self.connection_token = self.connection_result["token"]
            usr = self.preferences["username"] if "username" in self.preferences.keys() else self.__tr("", "anonimo")
            self.statusbar.showMessage(self.__tr("anon_conn", "Connesso a opensubtitles.org come %s." % usr))
        except xmlrpc.client.ProtocolError as e:
            popup = SubFetcherPopupWindow(self.__tr("conn_status_error",
                                                    "Errore di connessione irreversibile: riprova dopo."),
                                          'e', self)
            popup.exec_()
            log("There was a connection error, see [ERR_MSG]", "ERR", e.errmsg, e.errcode)
            self.close()

        # This timer will be used to keep alive the connection
        self.timer = QtCore.QTimer(self)

        # Slots connection
        self.btnAbout.clicked.connect(self.open_about)
        self.btnBrowseSearchFolder.clicked.connect(self.select_search_folder)
        self.btnBrowseSaveFolder.clicked.connect(self.select_save_folder)
        self.btnSearch.clicked.connect(self.scan_searchline_folder)
        self.btnDownloadSelected.clicked.connect(self.if_partial_selection)
        self.btnDownloadAll.clicked.connect(self.if_full_selection)
        self.btnLogIn.clicked.connect(lambda: self.log_in(self.usernameLine.text(), self.passwordLine.text()))
        self.saveFolderBtnGroup.buttonClicked.connect(self.select_save_folder)
        self.searchLine.textEdited.connect(self.on_searchline_edit)
        self.subtitlesLanguageComboBox.currentIndexChanged.connect(self.on_lang_combobox_change)
        self.tableView.clicked.connect(self.__display_selected_subtitle_info)
        self.timer.timeout.connect(lambda: self.connection.NoOperation(self.connection_token))
        self.timer.start(1000*60*15)

    def __display_selected_subtitle_info(self):
        selected_index = self.tableView.selectionModel().selectedRows()
        if len(selected_index) != 1:
            pass
        else:
            selected_video = self.subtitles_model.video_list[selected_index[0].row()]
            self.lblSeriesText.setText(selected_video["MovieNameSeries"])
            self.lblSeasonText.setText(str(selected_video["SeriesSeason"]))
            self.lblEpisodeText.setText(str(selected_video["SeriesEpisode"]))
            self.lblTitleText.setText(selected_video["SeriesEpisodeTitle"])
            self.lblIMDBText.setText("<a href='http://imdb.com/title/tt%s'>Link</a>" % selected_video["IDMovieImdb"])

    def __download_and_extract(self, videos):
        sub_paths = {}
        if self.preferences["save_folder"] is None:
            for v in videos:
                sub_paths[v.subtitle_id] = v.original_file_abspath.rsplit(".", 1)[0] + ".srt"
        else:
            for v in videos:
                pattern = re.compile("([\w\d]*)[/|\\\\]([\w\d\.\\[\\]-]+)\.([\w\d]+$)")
                file_name = re.search(pattern, v.original_file_abspath).group(2) + ".srt"
                sub_paths[v.subtitle_id] = self.preferences["save_folder"] + "/" + file_name
        try:
            subtitles_list = self.connection.DownloadSubtitles(self.connection_token, list(sub_paths.keys()))
            if subtitles_list["status"] == "200 OK":
                self.statusbar.showMessage(self.__tr("dwnld_ok", "Download dei file zippati riuscito"))
                for item in subtitles_list["data"]:
                    with open(sub_paths[item["idsubtitlefile"]], "wb") as srt_file:
                        srt_file.write(gzip.decompress(binascii.a2b_base64(item["data"])))
                        self.statusbar.showMessage(self.__tr("write_sub", "Scrivo il file %s" % srt_file.name))
                self.statusbar.showMessage(self.__tr("dwnld_complete", "Completato!"))
                return True
            else:
                raise xmlrpc.client.ProtocolError
        except xmlrpc.client.ProtocolError as e:
            message = self.__tr("conn_err_human", "<p>C'è stato un errore di connessione.</p>"
                                "<p>Alcuni sottotitoli potrebbero non essere stati scaricati</p>")
            self.statusbar.showMessage(self.__tr("dwnld_err", "Errore durante il download!"))
            p = SubFetcherPopupWindow(message, "e")
            p.exec_()
            log("Connection error while trying to download gzipped subtitles.", "ERR", e.errmsg, e.errcode)

    def closeEvent(self, event):
        """Reimplemented closeEvent to ensure that settings are saved"""
        with open(os.getcwd()+"/prefs.json", "w") as w:
            if self.rememberMeCheclBox.isChecked():
                self.preferences["username"] = self.usernameLine.text()
            w.write(json.dumps(self.preferences))
        self.connection.LogOut(self.connection_token)
        event.accept()

    def if_full_selection(self):
        self.statusbar.showMessage(self.__tr("start_download", "Tento di scaricare %d sottotitoli" %
                                             self.subtitles_model.rowCount()))
        videos = self.scanned_videos
        if is_online():
            self.__download_and_extract(videos)
        else:
            p = SubFetcherPopupWindow(
                msg=self.__tr("conn_absent", "Connessione assente. Controlla il collegamento"),
                type="i",
                parent=self.parent
            )
            p.show()

    def if_partial_selection(self):
        videos = []
        self.statusbar.showMessage(self.__tr("start_download", "Tento di scaricare %d sottotitoli" %
                                             len(self.tableView.selectionModel().selectedRows())))
        for i in self.tableView.selectionModel().selectedRows():
            videos.append(self.scanned_videos[i.row()])
        if is_online():
            self.__download_and_extract(videos)
        else:
            p = SubFetcherPopupWindow(
                msg=self.__tr("conn_absent", "Connessione assente. Controlla il collegamento"),
                type="i",
                parent=self.parent
            )
            p.show()

    def log_in(self, usr, pwd):
        if is_online():
            results = self.connection.LogIn(usr, pwd, self.subtitles_language, self.OS_user_agent)
            if results["status"] != "200 OK":
                self.lblLogInResultIcon.setPixmap(QtGui.QPixmap(":/icons/icons/small_ui/error.png"))
                self.statusbar.showMessage(self.__tr("anon_conn", "Connesso a opensubtitles.org come anonimo."))
            else:
                self.lblLogInResultIcon.setPixmap(QtGui.QPixmap(":/icons/icons/small_ui/accept.png"))
                self.statusbar.showMessage(self.__tr("auth_conn", "Connesso a opensubtitles.org come %s." % usr))
            return results
        else:
            p = SubFetcherPopupWindow(
                msg=self.__tr("conn_absent", "Connessione assente. Controlla il collegamento"),
                type="e",
                parent=self
            )
            p.exec_()
            sys.exit()

    def on_lang_combobox_change(self):
        """Slot that changes language to search subtitles in"""
        self.subtitles_language = self.subtitlesLanguageComboBox.itemData(self.subtitlesLanguageComboBox.currentIndex())
        self.subtitles_language_full_name = self.language_codes[self.subtitles_language]
        self.statusbar.showMessage(self.__tr("sub_lang_changed_statusbar",
                                             "I sottotitoli verrano cercati per la lingua %s" %
                                             self.subtitles_language_full_name))
        self.preferences["subtitles_language"] = self.subtitles_language

    def on_searchline_edit(self):
        """Slot that sets folder_to_scan to what text is present in searchLine"""
        self.folder_to_scan = self.searchLine.text()

    def open_about(self):
        """Slot to open the 'about' window"""
        self.aboutWindow.show()

    def scan_searchline_folder(self):
        """Calls the thread to handle the scan after checking the provided path"""
        self.btnSearch.setEnabled(False)
        if self.folder_to_scan != "":
            if not os.path.isabs(self.folder_to_scan):
                example_path = ""
                if sys.platform == "win32":
                    example_path = r"C:\percorso\a\cartella"
                else:
                    example_path = "/percorso/a/cartella"
                popup = SubFetcherPopupWindow(self.__tr("path_is_not_absolute",
                                                        "<p>Il percorso deve essere " +
                                                        "assoluto e non relativo.</p><p>Es.:<br />" +
                                                        example_path + "</p>"),
                                              'e', self)
                popup.show()
            elif not os.path.isdir(self.folder_to_scan):
                popup = SubFetcherPopupWindow(self.__tr("selection_is_not_folder",
                                                        "<p>Il percorso inserito non è una cartella</p>"),
                                              'e', self)
                popup.show()
            else:
                self.statusbar.showMessage(self.__tr("start_search", "Inizio analisi dalla cartella %s" %
                                                     self.folder_to_scan))
                self.at_least_one_match = False

                # Create thread, let the worker scan folders and get data then destroy both
                self.scanned_videos = {}
                self.thread = QtCore.QThread()
                self.worker = SearchWorker(self.folder_to_scan, self)
                self.worker.update_statusbar.connect(self.statusbar.showMessage)
                self.worker.scan_complete.connect(self.update_tableView)
                self.worker.moveToThread(self.thread)
                self.thread.started.connect(self.worker.run)
                self.worker.scan_complete.connect(self.thread.quit)
                self.worker.scan_complete.connect(self.worker.deleteLater)
                self.thread.finished.connect(self.thread.deleteLater)
                self.thread.start()
        else:
            popup = SubFetcherPopupWindow(self.__tr("no_folder_selected", "<p>Nessuna cartella selezionata</p>"),
                                          'e', self)
            popup.show()
            self.btnSearch.setEnabled(True)

    def select_save_folder(self):
        if self.saveFolderBtnGroup.checkedButton() == self.rbtnSamePathAsVideo:
            self.preferences["save_folder"] = None
            self.saveFolderLine.setEnabled(False)
            self.btnBrowseSaveFolder.setEnabled(False)
        else:
            self.preferences["save_folder"] = QFileDialog.getExistingDirectory(self,
                                                               caption=self.__tr("open_diag_caption",
                                                                                 "Cartella dove salvare i sottotitoli"),
                                                               directory=self.saveFolderLine.text())
            self.saveFolderLine.setText(self.preferences["save_folder"])
            self.saveFolderLine.setEnabled(True)
            self.btnBrowseSaveFolder.setEnabled(True)

    def select_search_folder(self):
        """Slot that opens a file dialog to select the directory to scan and sets it to folder_to_scan and as
        text for searchLine"""
        self.folder_to_scan = QFileDialog.getExistingDirectory(self,
                                                               caption=self.__tr("open_diag_caption",
                                                                                 "Seleziona cartella da analizzare"),
                                                               directory=self.searchLine.text())
        self.searchLine.setText(self.folder_to_scan)

    def update_tableView(self):
        if self.at_least_one_match:
            # The for loops are necessary because otherwise the first subs will be pushed down, resulting in
            # a descending order for episodes.
            unpacked_video_list = list(self.scanned_videos.values())
            if self.subtitles_model.rowCount() == 0:
                for i in range(len(unpacked_video_list)):
                    if unpacked_video_list[i]["IDSubtitleFile"] != "":
                        self.subtitles_model.insertRow(i, None, unpacked_video_list[i])
            else:
                self.subtitles_model.removeRows(0, self.subtitles_model.rowCount(), None)
                for i in range(len(unpacked_video_list)):
                    if unpacked_video_list[i]["IDSubtitleFile"] != "":
                        self.subtitles_model.insertRow(i, None, unpacked_video_list[i])
            self.statusbar.showMessage(self.__tr("scan_ok", "Scansione riuscita in %s" % self.folder_to_scan))
            self.tableView.resizeRowsToContents()
            self.tableView.resizeColumnsToContents()
            self.tableView.sortByColumn(3, QtCore.Qt.AscendingOrder)
        else:
            self.statusbar.showMessage(self.__tr("scan_not_ok", "Nessun file in %s è stato scansionato" %
                                                 self.folder_to_scan))
        self.btnSearch.setEnabled(True)


class SubFetcherAboutWindow(QDialog, Ui_aboutWindow):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)


class SubFetcherPopupWindow(QDialog, Ui_popupDialog):
    def __init__(self, msg, type, parent=None):
        """Generic popup window

        :param msg: Message to be displayed
        :type msg: str
        :param type: one in i, e or q. Tells the type of the popup (info, error or question)
        :type type: str
        :param parent: parent widget
        :type parent: QWidget
        :return: nothing
        :rtype: None
        """
        QDialog.__init__(self, parent)
        self._translate = QtCore.QCoreApplication.translate
        if type == "i":
            self.icon = QtGui.QPixmap(":/icons/icons/big_ui/info.png")
            self.setWindowTitle(self._translate("infoWindow", "Informazione"))
        elif type == "e":
            self.icon = QtGui.QPixmap(":/icons/icons/big_ui/error.png")
            self.setWindowTitle(self._translate("warningWindow", "Attenzione!"))
        elif type == "q":
            self.icon = QtGui.QPixmap(":/icons/icons/big_ui/question.png")
            self.setWindowTitle(self._translate("errorWindow", "Domanda"))
        self.setupUi(self)
        self.lblIcon.setPixmap(self.icon)
        self.lblText.setText(msg)


def hashfile(name):
    try:

        longlongformat = 'q'  # long long
        bytesize = struct.calcsize(longlongformat)

        f = open(name, "rb")

        filesize = os.path.getsize(name)
        hash = filesize

        if filesize < 65536 * 2:
            return "SizeError"

        for x in range(int(65536 / bytesize)):
            buffer = f.read(bytesize)
            (l_value,) = struct.unpack(longlongformat, buffer)
            hash += l_value
            hash = hash & 0xFFFFFFFFFFFFFFFF  # to remain as 64bit number

        f.seek(max(0, filesize - 65536), 0)
        for x in range(int(65536 / bytesize)):
            buffer = f.read(bytesize)
            (l_value,) = struct.unpack(longlongformat, buffer)
            hash += l_value
            hash = hash & 0xFFFFFFFFFFFFFFFF

        f.close()
        returnedhash = "%016x" % hash
        return returnedhash

    except IOError:
        return "IOError"


def md5sum(filename):
    BLOCKSIZE = 65536
    if os.path.isfile(filename):
        with open(filename, "rb") as f:
            m = hashlib.md5()
            chunk = f.read(BLOCKSIZE)
            while len(chunk) > 0:
                m.update(chunk)
                chunk = f.read(BLOCKSIZE)
            return m.hexdigest()
