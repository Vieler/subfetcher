# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\aboutWindow.ui'
#
# Created: Sun Dec  7 14:02:24 2014
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from src import res_rc

class Ui_aboutWindow(object):
    def setupUi(self, aboutWindow):
        aboutWindow.setObjectName("aboutWindow")
        aboutWindow.resize(274, 512)
        aboutWindow.setMinimumSize(QtCore.QSize(0, 512))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/big_ui/srt_512.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        aboutWindow.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(aboutWindow)
        self.verticalLayout.setObjectName("verticalLayout")
        self.iconAbout = QtWidgets.QLabel(aboutWindow)
        self.iconAbout.setMinimumSize(QtCore.QSize(256, 256))
        self.iconAbout.setText("")
        self.iconAbout.setPixmap(QtGui.QPixmap(":/icons/icons/big_ui/srt_256.png"))
        self.iconAbout.setAlignment(QtCore.Qt.AlignCenter)
        self.iconAbout.setObjectName("iconAbout")
        self.verticalLayout.addWidget(self.iconAbout)
        self.groupBoxInfo = QtWidgets.QGroupBox(aboutWindow)
        self.groupBoxInfo.setObjectName("groupBoxInfo")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.groupBoxInfo)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.labelInfo = QtWidgets.QLabel(self.groupBoxInfo)
        self.labelInfo.setTextFormat(QtCore.Qt.RichText)
        self.labelInfo.setWordWrap(True)
        self.labelInfo.setOpenExternalLinks(True)
        self.labelInfo.setObjectName("labelInfo")
        self.verticalLayout_3.addWidget(self.labelInfo)
        self.verticalLayout.addWidget(self.groupBoxInfo)
        self.groupBoxCredits = QtWidgets.QGroupBox(aboutWindow)
        self.groupBoxCredits.setObjectName("groupBoxCredits")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBoxCredits)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.labelCredit = QtWidgets.QLabel(self.groupBoxCredits)
        self.labelCredit.setWordWrap(True)
        self.labelCredit.setOpenExternalLinks(True)
        self.labelCredit.setObjectName("labelCredit")
        self.verticalLayout_2.addWidget(self.labelCredit)
        self.verticalLayout.addWidget(self.groupBoxCredits)

        self.retranslateUi(aboutWindow)
        QtCore.QMetaObject.connectSlotsByName(aboutWindow)

    def retranslateUi(self, aboutWindow):
        _translate = QtCore.QCoreApplication.translate
        aboutWindow.setWindowTitle(_translate("aboutWindow", "Informazioni"))
        self.groupBoxInfo.setTitle(_translate("aboutWindow", "Informazioni"))
        self.labelInfo.setText(_translate("aboutWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Sub Fetcher</span> è scritto interamente in <a href=\"https://www.python.org/\"><span style=\" text-decoration: underline; color:#0000ff;\">Python 3</span></a> e <a href=\"http://www.riverbankcomputing.com/software/pyqt/download5\"><span style=\" text-decoration: underline; color:#0000ff;\">PyQt5</span></a> da Marcello Massaro con &lt;3.</p></body></html>"))
        self.groupBoxCredits.setTitle(_translate("aboutWindow", "Crediti"))
        self.labelCredit.setText(_translate("aboutWindow", "<html><head/><body><p>L\'<span style=\" font-weight:600;\">icona</span> di Sub Fetcher è disegnata da <a href=\"http://www.freepik.com\"><span style=\" text-decoration: underline; color:#0000ff;\">Freepik</span></a> ed ottenuta da <a href=\"http://www.flaticon.com\"><span style=\" text-decoration: underline; color:#0000ff;\">Flaticon</span></a>, rilasciata con Licenza <a href=\"http://creativecommons.org/licenses/by/3.0/\"><span style=\" text-decoration: underline; color:#0000ff;\">CC BY 3.0</span></a>. Le altre icone sono frutto del lavoro di  <a href=\"http://www.famfamfam.com/\"><span style=\" text-decoration: underline; color:#0000ff;\">Famfamfam</span></a> (parte di Silk Icon set e Flag icon), <a href=\"https://www.iconfinder.com/iconsets/ose\"><span style=\" text-decoration: underline; color:#0000ff;\">Gakuseisean</span></a> (parte di Ose icon set).</p><p>I <span style=\" font-weight:600;\">sottotitoli</span> vengono recuperati grazie al grande lavoro di <a href=\"http://www.opensubtitles.org\"><img src=\":/icons/icons/OS_favicon.ico\"/></a><a href=\"http://www.opensubtitles.org\"><span style=\" text-decoration: underline; color:#0000ff;\"> OpenSubtitles</span></a> e alle loro API.</p></body></html>"))


