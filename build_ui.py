#!/usr/bin/python3
# -*- coding: utf-8 -*-
import hashlib
import os
import subprocess
import yaml
from PyQt5.uic import *


def md5sum(filename):
    BLOCKSIZE = 65536
    if os.path.isfile(filename):
        with open(filename, "rb") as f_open:
            m = hashlib.md5()
            chunk = f_open.read(BLOCKSIZE)
            while len(chunk) > 0:
                m.update(chunk)
                chunk = f_open.read(BLOCKSIZE)
            return str(m.hexdigest())


if __name__ == '__main__':
    md5_list = yaml.load(open('md5sum.yaml').read())
    for f in md5_list.keys():
        if f != 'res.qrc':
            new_md5_sum = md5sum('ui/'+f)
            if md5_list[f] == new_md5_sum:
                pass
            else:
                md5_list[f] = new_md5_sum
                with open('ui/'+f, 'r') as ui_file:
                    with open('src/'+f.replace('.ui', '.py'), 'w') as py_file:
                        print(ui_file, py_file)
                        compileUi(ui_file, py_file)
                        print("File %s was found to be modified and has been recompiled." % f)
    res_md5_sum = md5sum('ui/res.qrc')
    if md5_list['res.qrc'] == res_md5_sum:
        pass
    else:
        md5_list['res.qrc'] = res_md5_sum
        subprocess.call('pyrcc5 -o src/res_rc.py ui/res.qrc')
        print("File res.qrc was found to be modified and has been recompiled.")
    yaml.dump(md5_list, open('md5sum.yaml', 'w'), default_flow_style=False)