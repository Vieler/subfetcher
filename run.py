import sys
import os
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTranslator
import src.subFetcher


if __name__ == '__main__':
    if sys.platform == 'win32':
        # This is a hack found here
        # http://stackoverflow.com/questions/1551605/how-to-set-applications-taskbar-icon-in-windows-7
        # that will take care of putting the right icon in windows taskbar
        import ctypes

        app_id = 'marcellomassaro.sub_fetcher'
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)

    app = QApplication(sys.argv)
    # translator = QtCore.QTranslator()
    # translator.load("subfetcher_%s" % loc)
    windUI = src.subFetcher.SubFetcherMainWindow()
    windUI.show()

    sys.exit(app.exec_())